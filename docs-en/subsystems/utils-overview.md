# Utils Overview<a name="EN-US_TOPIC_0000001060172974"></a>

The Utils library stores common basic components of OpenHarmony. These basic components can be used by OpenHarmony service subsystems and upper-layer applications.

The Utils library provides the following capabilities on different platforms:

LiteOS Cortex-M \(Hi3861 platform\): KV store, file operations, and IoT peripheral control

LiteOS Cortex-A \(Hi3516 or Hi3518 platform\): JavaScript APIs for KV store, timer, and data and file storage modules


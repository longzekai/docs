# Third-Party Library Porting Guide

-   [Third-Party Library Porting Guide](third-party-library-porting-guide.md)
    -   [Overview](overview.md)
    -   [Porting a Library Built Using CMake](porting-a-library-built-using-cmake.md)
    -   [Porting a Library Built Using Makefile](porting-a-library-built-using-makefile.md)

